# Change Log

## RELEASE 0.0.4 - 2017-07-05
### Fixed
- Technical - Use res.send instead of res.sendStatus to prevent warning on console.
